import pandas as pd
from pandas.plotting import scatter_matrix
import numpy as np

from sklearn.datasets import load_iris
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import matplotlib.pyplot as plt

iris_dataset = load_iris()

# print("Ключі iris_dataset: {}".format(iris_dataset.keys()))
# print(iris_dataset['DESCR'][:193] + "...")
# print("Назви відповідей: \n{}".format(iris_dataset['target_names']))
# print("Назва ознак: {}".format(iris_dataset['feature_names']))
# print("Тип масиву data: {}".format(type(iris_dataset['data'])))
# print("Форма масиву data: {}".format(iris_dataset['data'].shape))
# print("Значення ознак для перших п'яти прикладів:")
# print(iris_dataset['data'][:5])
# print("Тип масиву target: {}".format(type(iris_dataset['target'])))
# print("Відповіді:\n{}".format(iris_dataset['target']))


# fig, axes = plt.subplots(2, 2, figsize=(10, 6))
# features = iris_dataset.feature_names
#
# for i, ax in enumerate(axes.ravel()):
#     ax.boxplot(iris_dataset.data[:, i], vert=False)
#     ax.set_title(features[i])
#     ax.set_yticklabels([])
#
#     ax.hist(iris_dataset.data[:, i], bins=20, color='skyblue', edgecol-or='black')
#     ax.set_title(features[i])
#     ax.set_xlabel("Значення")
#     ax.set_ylabel("Частота")
#
# plt.tight_layout()
# plt.show()

X = iris_dataset.data
y = iris_dataset.target

iris_df = pd.DataFrame(data=X, columns=iris_dataset.feature_names)
iris_df['class'] = y

scatter_matrix(iris_df, c=iris_df['class'], figsize=(12, 8), marker='o', alpha=0.8, hist_kwds={'bins': 20})
plt.suptitle("Матриця діаграм розсіювання", fontsize=16)
plt.show()


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=1)

models = []
models.append(('LR', LogisticRegression(solver='liblinear', multi_class='ovr')))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC(gamma='auto')))

results = []
names = []
for name, model in models:
    kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
    cv_results = cross_val_score(model, X_train, y_train, cv=kfold, scoring='accuracy')
    results.append(cv_results)
    names.append(name)
    print('%s: %f (%f)' % (name, cv_results.mean(), cv_results.std()))

plt.boxplot(results, labels=names)
plt.title('Algorithm Comparison')
plt.show()

model = SVC(gamma='auto')
model.fit(X_train, y_train)
predictions = model.predict(X_test)

print(accuracy_score(y_test, predictions))
print(confusion_matrix(y_test, predictions))
print(classification_report(y_test, predictions))

# Створення нового спостереження
X_new = np.array([[5.0, 2.9, 1.0, 0.2]])
print("Форма масиву X_new: {}".format(X_new.shape))

# Прогноз для нового спостереження
prediction = model.predict(X_new)
print("Прогноз: {}".format(prediction))
print("Спрогнозована метка: {}".format(iris_dataset.target_names[prediction]))
