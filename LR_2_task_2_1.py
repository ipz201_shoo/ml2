import numpy as np
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

# Вхідний файл, який містить дані
input_file = 'income_data.txt'

# Читання даних та підготовка до класифікації
X = []
y = []
count_class1 = 0
count_class2 = 0
max_datapoints = 25000

# Зчитування даних з файлу та підготовка до класифікації
with open(input_file, 'r') as f:
    for line in f.readlines():
        if count_class1 >= max_datapoints and count_class2 >= max_datapoints:
            break
        if '?' in line:
            continue
        data = line[:-1].split(', ')
        if data[-1] == '<=50K' and count_class1 < max_datapoints:
            X.append(data)
            count_class1 += 1
        if data[-1] == '>50K' and count_class2 < max_datapoints:
            X.append(data)
            count_class2 += 1

# Перетворення на масив numpy
X = np.array(X)

# Перетворення рядкових даних на числові
label_encoder = []
X_encoded = np.empty(X.shape)
for i, item in enumerate(X[0]):
    if item.isdigit():
        X_encoded[:, i] = X[:, i]
    else:
        label_encoder.append(preprocessing.LabelEncoder())
        X_encoded[:, i] = label_encoder[-1].fit_transform(X[:, i])

X = X_encoded[:, :-1].astype(int)
y = X_encoded[:, -1].astype(int)

# Розбивка даних на навчальний та тестовий набори
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
print(1)

# Поліноміальне ядро SVM
poly_svc = SVC(kernel='poly', degree=2)  # Встановіть ступінь полінома, наприклад, 3
poly_svc.fit(X_train, y_train)
y_pred_poly = poly_svc.predict(X_test)
print(2)

# Гаусове (RBF) ядро SVM
rbf_svc = SVC(kernel='rbf')
rbf_svc.fit(X_train, y_train)
y_pred_rbf = rbf_svc.predict(X_test)
print(3)

# Сигмоїдальне ядро SVM
sigmoid_svc = SVC(kernel='sigmoid')
sigmoid_svc.fit(X_train, y_train)
y_pred_sigmoid = sigmoid_svc.predict(X_test)
print(4)

# Оцінка якості класифікації для кожного ядра
def evaluate_classifier(y_true, y_pred, kernel_name):
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred, average='weighted')
    recall = recall_score(y_true, y_pred, average='weighted')
    f1 = f1_score(y_true, y_pred, average='weighted')
    print(f"Kernel: {kernel_name}")
    print(f"Accuracy: {accuracy:.2f}")
    print(f"Precision: {precision:.2f}")
    print(f"Recall: {recall:.2f}")
    print(f"F1 Score: {f1:.2f}")
    print("")

evaluate_classifier(y_test, y_pred_poly, "Polynomial Kernel")
evaluate_classifier(y_test, y_pred_rbf, "RBF Kernel")
evaluate_classifier(y_test, y_pred_sigmoid, "Sigmoid Kernel")
